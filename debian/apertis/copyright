Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2007-2012, Stefan Walter
License: LGPL-2+

Files: egg/fixtures/* pkcs11/gkm/fixtures/* pkcs11/gnome2-store/fixtures/* pkcs11/secret-store/fixtures/*
Copyright: 2003 Red Hat, Inc
 2007 Nate Nielsen
 2007-2008 Stefan Walter
License: LGPL-2+

Files: INSTALL
Copyright: 1994-1996, 1999-2002, 2004-2013, Free Software Foundation
License: FSFAP

Files: aclocal.m4
Copyright: 1996-2021, Free Software Foundation, Inc.
License: FSFULLR and/or GPL-2+

Files: build/*
Copyright: 1996-2021, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: build/config.rpath
Copyright: 1996-2016, Free Software Foundation, Inc.
License: FSFULLR

Files: build/install-sh
Copyright: 1994, X Consortium
License: X11

Files: build/ltmain.sh
Copyright: 1996-2015, Free Software Foundation, Inc.
License: (GPL-2+ and/or GPL-3+) with Libtool exception

Files: build/m4/*
Copyright: 1996-2016, Free Software Foundation, Inc.
License: FSFULLR

Files: build/m4/gettext.m4
 build/m4/intlmacosx.m4
 build/m4/po.m4
 build/m4/progtest.m4
Copyright: 1995-2016, Free Software Foundation, Inc.
License: FSFULLR and/or GPL and/or LGPL

Files: build/m4/libtool.m4
Copyright: 1996-2001, 2003-2015, Free Software Foundation, Inc.
License: (FSFULLR and/or GPL-2) with Libtool exception

Files: build/m4/ltoptions.m4
 build/m4/ltsugar.m4
 build/m4/lt~obsolete.m4
Copyright: 2004, 2005, 2007-2009, 2011-2015, Free Software
License: FSFULLR

Files: build/m4/nls.m4
Copyright: 1995-2003, 2005, 2006, 2008-2014, 2016, Free Software
License: FSFULLR and/or GPL and/or LGPL

Files: build/tap-driver
Copyright: 2013, 2017-2019, Red Hat, Inc.
License: LGPL-2.1+

Files: configure
Copyright: 1992-1996, 1998-2017, 2020, 2021, Free Software Foundation
License: FSFUL

Files: daemon/*
Copyright: 2008-2011, Stefan Walter
License: LGPL-2.1+

Files: daemon/dbus/gkd-dbus-environment.c
 daemon/dbus/gkd-dbus-private.h
 daemon/dbus/gkd-dbus-secrets.c
 daemon/dbus/gkd-dbus-session.c
 daemon/dbus/gkd-dbus.c
 daemon/dbus/gkd-dbus.h
 daemon/dbus/test-dbus-util.c
Copyright: 2007-2012, Stefan Walter
License: LGPL-2+

Files: daemon/dbus/gkd-secret-exchange.c
 daemon/dbus/gkd-secret-exchange.h
Copyright: 2010, 2011, Collabora Ltd.
License: LGPL-2.1+

Files: daemon/dbus/gkd-secret-portal.c
 daemon/dbus/gkd-secret-portal.h
Copyright: 2013, 2017-2019, Red Hat, Inc.
License: LGPL-2.1+

Files: daemon/dbus/org.freedesktop.impl.portal.Request.xml
 daemon/dbus/org.freedesktop.impl.portal.Secret.xml
Copyright: 2016, 2019, Red Hat, Inc.
License: LGPL-2+

Files: daemon/dbus/test-dbus-items.c
 daemon/dbus/test-dbus-lock.c
 daemon/dbus/test-dbus-search.c
 daemon/dbus/test-dbus-signals.c
 daemon/dbus/test-service.c
 daemon/dbus/test-service.h
Copyright: 2012-2014, Red Hat Inc
License: LGPL-2+

Files: daemon/dbus/test-dbus-portal.c
Copyright: 2013-2019, Red Hat, Inc
License: LGPL-2+

Files: daemon/gkd-capability.c
Copyright: 2011, 2020, Steve Grubb
License: LGPL-2.1+

Files: daemon/gkd-capability.h
Copyright: 2010, Yaron Sheffer
License: LGPL-2.1+

Files: daemon/gkd-main.c
Copyright: 2003, Red Hat, Inc
License: GPL-2+

Files: daemon/gkd-test.c
 daemon/gkd-test.h
 daemon/test-shutdown.c
 daemon/test-startup.c
Copyright: 2012-2014, Red Hat Inc
License: LGPL-2+

Files: daemon/gkd-util.c
 daemon/gkd-util.h
Copyright: 2007-2012, Stefan Walter
License: LGPL-2+

Files: daemon/login/*
Copyright: 2013, 2017-2019, Red Hat, Inc.
License: LGPL-2.1+

Files: daemon/login/gkd-login.c
 daemon/login/gkd-login.h
Copyright: 2008-2011, Stefan Walter
License: LGPL-2.1+

Files: daemon/ssh-agent/*
Copyright: 2013, 2017-2019, Red Hat, Inc.
License: LGPL-2.1+

Files: daemon/ssh-agent/gkd-ssh-agent-preload.c
 daemon/ssh-agent/gkd-ssh-agent-preload.h
 daemon/ssh-agent/gkd-ssh-agent-process.c
 daemon/ssh-agent/gkd-ssh-agent-process.h
 daemon/ssh-agent/gkd-ssh-agent-util.c
Copyright: 2018, Red Hat, Inc.
 2014, Stef Walter
License: LGPL-2.1+

Files: daemon/ssh-agent/gkd-ssh-agent-private.h
Copyright: 2007, Stefan Walter
License: GPL-2+

Files: daemon/ssh-agent/gkd-ssh-agent-service.c
 daemon/ssh-agent/gkd-ssh-agent-service.h
Copyright: 2018, Red Hat, Inc.
 2007, Stefan Walter
License: LGPL-2.1+

Files: daemon/ssh-agent/test-gkd-ssh-agent-util.c
Copyright: 2008-2011, Stefan Walter
License: LGPL-2.1+

Files: debian/*
Copyright: 2007-2011, Collabora Ltd
            1991-1999, Free Software Foundation, Inc
            2007, Nokia Corporation
            1995-1997, Peter Mattis, Spencer Kimball and Josh MacDonald
            2017-2018, Red Hat, Inc
            2014, Stef Walter
            2007-2011, Stefan Walter
            2011, Steve Grubb
            2010, Yaron Sheffer
License: GPL-2+

Files: egg/dotlock.c
 egg/dotlock.h
Copyright: 1998, 2000, 2001, 2003-2006, 2008, 2010, 2011, Free Software Foundation, Inc.
License: BSD-3-clause

Files: egg/egg-asn1-defs.c
 egg/test-hkdf.c
Copyright: 2011, Collabora Ltd.
License: LGPL-2+

Files: egg/egg-asn1x.c
Copyright: 2008, 2009, Stefan Walter
License: LGPL-2+ and/or LGPL-2.1+

Files: egg/egg-byte-array.c
 egg/egg-byte-array.h
Copyright: 2010, Collabora Ltd
License: LGPL-2.1+

Files: egg/egg-dh.c
 egg/egg-dh.h
 egg/egg-error.h
 egg/egg-hex.c
 egg/egg-hex.h
 egg/egg-libgcrypt.c
 egg/egg-libgcrypt.h
 egg/egg-padding.c
 egg/egg-padding.h
 egg/egg-symkey.c
 egg/egg-symkey.h
 egg/egg-testing.h
Copyright: 2008-2011, Stefan Walter
License: LGPL-2.1+

Files: egg/egg-hkdf.c
 egg/egg-hkdf.h
 egg/egg-testing.c
Copyright: 2010, 2011, Collabora Ltd.
License: LGPL-2.1+

Files: egg/egg-timegm.c
 egg/egg-timegm.h
Copyright: 1999, 2001, 2002, Free Software Foundation, Inc.
License: LGPL-2+

Files: egg/egg-unix-credentials.c
 egg/egg-unix-credentials.h
Copyright: 2008, Stefan Walter
License: BSD-3-clause

Files: egg/mock-interaction.c
 egg/mock-interaction.h
Copyright: 2011, Collabora Ltd
License: LGPL-2+

Files: egg/test-armor.c
Copyright: 2012, 2014, Red Hat Inc.
License: LGPL-2+

Files: pam/*
Copyright: 2007-2009, Stef Walter
License: LGPL-2+

Files: pam/mock-pam.c
Copyright: 2012, 2014, Red Hat Inc.
License: LGPL-2+

Files: pam/test-pam.c
Copyright: 2012-2014, Red Hat Inc
License: LGPL-2+

Files: pkcs11/*
Copyright: 2008-2011, Stefan Walter
License: LGPL-2.1+

Files: pkcs11/gkm/gkm-assertion.h
Copyright: 2010, Stefan Walter
 2010, Collabora Ltd
License: LGPL-2.1+

Files: pkcs11/gkm/gkm-data-asn1.c
 pkcs11/gkm/gkm-data-asn1.h
 pkcs11/gkm/gkm-data-der.c
 pkcs11/gkm/gkm-data-der.h
 pkcs11/gkm/mock-module.c
 pkcs11/gkm/mock-module.h
 pkcs11/gkm/test-attributes.c
 pkcs11/gkm/test-certificate.c
 pkcs11/gkm/test-credential.c
 pkcs11/gkm/test-data-asn1.c
 pkcs11/gkm/test-data-der.c
 pkcs11/gkm/test-memory-store.c
 pkcs11/gkm/test-object.c
 pkcs11/gkm/test-secret.c
 pkcs11/gkm/test-sexp.c
 pkcs11/gkm/test-store.c
 pkcs11/gkm/test-timer.c
 pkcs11/gkm/test-transaction.c
Copyright: 2007-2012, Stefan Walter
License: LGPL-2+

Files: pkcs11/gkm/gkm-debug.c
Copyright: 2007, Nokia Corporation
 2007, Collabora Ltd.
License: LGPL-2.1+

Files: pkcs11/gkm/gkm-debug.h
Copyright: 2007-2011, Collabora Ltd.
 2007, Nokia Corporation
License: LGPL-2.1+

Files: pkcs11/gkm/gkm-ecdsa-mechanism.c
 pkcs11/gkm/gkm-ecdsa-mechanism.h
Copyright: 2013, 2017-2019, Red Hat, Inc.
License: LGPL-2.1+

Files: pkcs11/gkm/gkm-generic-key.c
 pkcs11/gkm/gkm-generic-key.h
 pkcs11/gkm/gkm-hkdf-mechanism.h
Copyright: 2010, 2011, Collabora Ltd.
License: LGPL-2.1+

Files: pkcs11/gkm/gkm-util.c
Copyright: 2008, 2009, Stefan Walter
License: LGPL-2+ and/or LGPL-2.1+

Files: pkcs11/gnome2-store/frob-gnome2-file.c
 pkcs11/gnome2-store/mock-gnome2-module.c
 pkcs11/gnome2-store/mock-gnome2-module.h
 pkcs11/gnome2-store/test-gnome2-file.c
 pkcs11/gnome2-store/test-gnome2-private-key.c
 pkcs11/gnome2-store/test-gnome2-storage.c
 pkcs11/gnome2-store/test-import.c
Copyright: 2007-2012, Stefan Walter
License: LGPL-2+

Files: pkcs11/gnome2-store/gkm-gnome2-standalone.c
Copyright: 2007-2009, Stef Walter
License: LGPL-2+

Files: pkcs11/pkcs11i.h
Copyright: 2007-2009, Stef Walter
License: LGPL-2+

Files: pkcs11/pkcs11n.h
Copyright: 1994-2000, the Initial Developer.
License: GPL-2+ and/or GPL-2+ or LGPL-2.1+ and/or MPL-1.1

Files: pkcs11/rpc-layer/*
Copyright: 2007-2009, Stef Walter
License: LGPL-2+

Files: pkcs11/rpc-layer/gkm-rpc-module.c
Copyright: 2007-2012, Stefan Walter
License: LGPL-2+

Files: pkcs11/rpc-layer/test-initialize.c
Copyright: 2012-2014, Red Hat Inc
License: LGPL-2+

Files: pkcs11/secret-store/dump-keyring0-format.c
 pkcs11/secret-store/gkm-secret-binary.c
Copyright: 2007, 2009, Stefan Walter
 2003, 2013, Red Hat, Inc
License: GPL-2+

Files: pkcs11/secret-store/gkm-secret-standalone.c
Copyright: 2007-2009, Stef Walter
License: LGPL-2+

Files: pkcs11/secret-store/gkm-secret-textual.c
 pkcs11/secret-store/mock-secret-module.c
 pkcs11/secret-store/mock-secret-module.h
 pkcs11/secret-store/test-secret-binary.c
 pkcs11/secret-store/test-secret-collection.c
 pkcs11/secret-store/test-secret-compat.c
 pkcs11/secret-store/test-secret-data.c
 pkcs11/secret-store/test-secret-fields.c
 pkcs11/secret-store/test-secret-item.c
 pkcs11/secret-store/test-secret-object.c
 pkcs11/secret-store/test-secret-search.c
 pkcs11/secret-store/test-secret-textual.c
Copyright: 2007-2012, Stefan Walter
License: LGPL-2+

Files: pkcs11/secret-store/test-secret-schema.c
Copyright: 2012, Red Hat Ltd.
License: LGPL-2+

Files: pkcs11/ssh-store/gkm-ssh-standalone.c
Copyright: 2007-2009, Stef Walter
License: LGPL-2+

Files: pkcs11/ssh-store/mock-ssh-module.c
 pkcs11/ssh-store/mock-ssh-module.h
 pkcs11/ssh-store/test-private-key.c
 pkcs11/ssh-store/test-ssh-openssh.c
Copyright: 2007-2012, Stefan Walter
License: LGPL-2+

Files: pkcs11/xdg-store/dump-trust-file.c
 pkcs11/xdg-store/frob-trust-file.c
Copyright: 2010, 2011, Collabora Ltd.
License: LGPL-2.1+

Files: pkcs11/xdg-store/gkm-xdg-asn1-defs.c
Copyright: 2011, Collabora Ltd.
License: LGPL-2+

Files: pkcs11/xdg-store/gkm-xdg-asn1-defs.h
 pkcs11/xdg-store/mock-xdg-module.h
 pkcs11/xdg-store/test-xdg-trust.c
Copyright: 2007-2012, Stefan Walter
License: LGPL-2+

Files: pkcs11/xdg-store/gkm-xdg-assertion.h
Copyright: 2010, Stefan Walter
 2010, Collabora Ltd
License: LGPL-2.1+

Files: pkcs11/xdg-store/gkm-xdg-standalone.c
Copyright: 2007-2009, Stef Walter
License: LGPL-2+

Files: pkcs11/xdg-store/mock-xdg-module.c
 pkcs11/xdg-store/test-xdg-module.c
Copyright: 2010, Stefan Walter
 2010, Collabora Ltd
License: LGPL-2+

Files: tool/gkr-tool-version.c
Copyright: 2011, Collabora Ltd.
License: LGPL-2+

Files: Makefile.in
Copyright: 1994-2017, Free Software Foundation, Inc
License: FSFULLR

Files: NEWS
Copyright: 2007-2011, Collabora Ltd
 1991-1999, Free Software Foundation, Inc
 2007, Nokia Corporation
 1995-1997, Peter Mattis, Spencer Kimball and Josh MacDonald
 2017-2018, Red Hat, Inc
 2014, Stef Walter
 2007-2011, Stefan Walter
 2011, Steve Grubb
 2010, Yaron Sheffer
License: GPL-2+

Files: daemon/gkd-pkcs11.c daemon/gkd-pkcs11.h
Copyright: 2003 Red Hat, Inc
 2007 Stefan Walter
License: GPL-2+

Files: egg/fixtures/test-pkcs12-1.der egg/fixtures/test-rsakey-1.der pkcs11/gkm/fixtures/der-key-PBE-MD5-DES.p8 pkcs11/gkm/fixtures/der-key-PBE-SHA1-3DES.p8 pkcs11/gkm/fixtures/der-key-PBE-SHA1-DES.p8 pkcs11/gkm/fixtures/der-key-PBE-SHA1-RC4-128.p8 pkcs11/gkm/fixtures/der-key-encrypted-pkcs5.p8 pkcs11/gkm/fixtures/der-key-v2-des3.p8 pkcs11/gkm/fixtures/test-certificate-2.der pkcs11/gkm/mock-locked-object.c pkcs11/gkm/mock-locked-object.h pkcs11/gnome2-store/fixtures/personal.p12 pkcs11/gnome2-store/fixtures/test-certificate.cer pkcs11/gnome2-store/fixtures/user.keystore pkcs11/pkcs11x.h
Copyright: 2003 Red Hat, Inc
 2007 Nate Nielsen
 2007-2008 Stefan Walter
License: LGPL-2+

Files: pkcs11/pkcs11.h
Copyright: 2006, 2007 g10 Code GmbH
 2006 Andreas Jellinghaus
License: custom-license
 This file is free software; as a special exception the author gives
 unlimited permission to copy and/or distribute it, with or without
 modifications, as long as this notice is preserved.
 .
 This file is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY, to the extent permitted by law; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.
